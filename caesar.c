/**
 * @file caesar.c
 * @author Jannik Vierling 1226434
 * @date 15.12.2013
 * 
 * @brief Implementation of a Caesar encoder
 * 
 * This file contains the implementation of a Caesar encoder, which reads
 * data from a shared memory segment and outputs it to its standard ouput.
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sem182.h>
#include <stdint.h>
#include "caesar.h"

/** @brief the maximum shift allowed for the encoding */
#define MAX_SHIFT (25)

/** @brief the minimum shift allowed for the encoding */
#define MIN_SHIFT (1)

/** 
 * @struct arguments
 * @brief Contains the command line arguments
 */
struct arguments {
	/** @var arguments::progname
	 * The programs name, as called from the command line */
	char *progname; 
	/** @var arguments::shift
  	 * The shift to encode data with */
	uint8_t shift;
};

/** @brief Holds the parsed command line arguments */
static struct arguments args = {.progname = "caesar", .shift = 0};

/**
 * @struct shm
 * @brief Contains data about a shared memory segment
 */
struct shm {
	/* @var shm::shmid
	 * The shared memory segments id */
	int shmid;
	/* @var shm::key
	 * The key the shared memory segment is associated with */
	key_t key;
	/* @var shm::shm
	 * Pointer to the address where the segment is attached */
	char * shm;
}; 

/** @brief The shared memory segment */
static struct shm shared = {.shmid = -1, .key = -1, .shm = NULL};

/** @brief ID of Semaphor 1 */
static int sem1 = -1;

/** @brief ID of Semaphor 2 */
static int sem2 = -1;

/**
 * @brief Encodes the message with Caesar encoding
 * @param shift The shift by which each character is shifted
 * @param message The message to be encoded. The messages content changes
 * after calling this function.
 * @return A pointer on message[0]
 */
static char * encode_caesar(int shift, char * message);

/**
 * @brief Prints an error message, and leaves the program with the code retval
 * @param retval The return code
 * @param fmt A format string for the error message
 * @return void
 * @details If errno is not equal to 0, this function also prints an errno
 * specific error message.
 */
static void bail_out(int retval, const char *fmt, ...);

/**
 * @brief Prints the synopsis of this program to stdout 
 * @return void
 */
static void usage(void);

/**
 * @brief Parses the command line arguments
 * @param argc The number of command line arguments
 * @param argv An array of command line arguments
 * @return void
 * @details saves the parsed arguments to ::args
 */
static void parse_args(int argc, char **argv);

/**
 * @brief Frees the ressources by this program
 * @param force If true all resources are freed regardless of synchronization
 * @return void
 * @details This function frees ::sem1, ::sem2, ::shared
 */
static void free_resources(int force);

/**
 * @brief Initializes the shared memory segment
 * @return void
 * @details This function might create a new segment
 */
static void init_shm(void);

/**
 * @brief Initializes the semaphores
 * @return void
 * @details This function tries to grab existing semaphores, if the semaphores
 * do not exist it will create them.
 */
static void init_sem(void);

/**
 * @brief Initializes the signal handler
 * @return void
 * @details The signal handler is initilized for SIGINT and SIGTERM
 */
static void init_signal_handler(void);

/**
 * @brief Forces the program to terminate
 * @param sig The signal number that triggered the execution of this function.
 * @return void
 */
static void terminate(int sig);

/**
 * @brief Programs entry point. This function reads one line at a time from
 * the shared memory segement and prints it ceasar-encoded to the stdout.
 * @param argc The number of command line arguments
 * @param argv An array of the command line arguments
 * @return The programs exit code
 */
int main(int argc, char **argv)
{
	int done = 0;
	char line[MAX_LINE_LEN];

	init_signal_handler();	

	parse_args(argc, argv);

	init_sem();
	init_shm();

	do {
		if (-1 == P(sem2)) {
			bail_out(EXIT_FAILURE, "P()");
		}
			strncpy(line, shared.shm, MAX_LINE_LEN);

		if (-1 == V(sem1)) {
			bail_out(EXIT_FAILURE, "V()");
		}

		if (0 == strcmp(EOFSTR, line)) {
			done = 1;
		} else if (-1 == printf("%s\n", encode_caesar(args.shift, line))) {
			bail_out(EXIT_FAILURE, "printf()");
		}
	}
	while (!done);

	free_resources(0);

	return EXIT_SUCCESS;
}

void init_sem(void)
{
	if (-1 == (sem1 = semgrab(SEMKEY))) {
		if (-1 == (sem1 = seminit(SEMKEY, PERMISSION, 1))) {
			bail_out(EXIT_FAILURE, "semgrab()");
		}
	}
	if (-1 == (sem2 = semgrab(SEMKEY+1))) {
		if (-1 == (sem2 = seminit(SEMKEY+1, PERMISSION, 0))) {
			bail_out(EXIT_FAILURE, "semgrab()");
		}
	}
}

void init_shm(void)
{
	/* create key */
	shared.key = ftok(SHMPATH, SHMPROJ);
	
	if (-1 == shared.key) {
		bail_out(EXIT_FAILURE, "ftok()");
	}	
	
	/* create segment */
	shared.shmid = shmget(shared.key, MAX_LINE_LEN * sizeof(char),
		IPC_CREAT | PERMISSION);

	if (-1 == shared.shmid) {
		bail_out(EXIT_FAILURE, "shmget()");
	}

	/* attach segment */
	shared.shm = shmat(shared.shmid, NULL, 0);

	if ((void *)-1 == shared.shm) {
		bail_out(EXIT_FAILURE, "shmat()");
	}
}

void parse_args(int argc, char **argv)
{
	char *endptr = NULL;

	if (argc != 2) {
		usage();
		bail_out(EXIT_FAILURE, "Invalid arguments");
	}
	
	args.progname = argv[0];
	args.shift    = (int) strtol(argv[1], &endptr, 10);

	if (0 != errno) {
		bail_out(EXIT_FAILURE, "invalid value for key");
	}
	if (MIN_SHIFT > args.shift || MAX_SHIFT < args.shift) {
		bail_out(EXIT_FAILURE, "key out of range [1,25]");
	}
}

void bail_out(int retval, const char *fmt, ...)
{
	va_list ap;

	(void) fprintf(stderr, "%s : ", args.progname);

	va_start(ap, fmt);	
	(void) vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (errno != 0) {
		(void) fprintf(stderr, " : %s", strerror(errno));
	}

	(void) fputc('\n', stderr);

	free_resources(1);

	exit(retval);
}

void free_resources(int force)
{
	(void) shmdt(shared.shm);

	if (force) {
		(void) semrm(sem1);
		(void) semrm(sem2);

		if (-1== shmctl(shared.shmid, IPC_RMID, NULL))
		{
			(void) fprintf(stderr, "error here\n");
		}
	}
}

void usage(void)
{
	(void) printf("usage : %s <key>\n", args.progname);
}

char * encode_caesar(int shift, char *message)
{
	char * tmp;

	for (tmp = message; '\0' != *tmp; tmp++) {
		*tmp = 'A' + (*tmp - 'A' + shift) % ('Z' - 'A' + 1);
	}

	return message;
}

void terminate(int sig)
{
	free_resources(1);
	
	(void) fprintf(stdout, "Caught signal %d. Terminating...\n",
			sig);
	
	exit(EXIT_FAILURE);
}

void init_signal_handler(void)
{
	struct sigaction sa;

	sa.sa_handler = terminate;
	
	if (-1 == sigemptyset(&(sa.sa_mask))) {
		bail_out(EXIT_FAILURE, "sigemptyset()");
	}

	if (-1 == sigaction(SIGINT, &sa, NULL)) {
		bail_out(EXIT_FAILURE, "sigaction()");
	}
	if (-1 == sigaction(SIGTERM, &sa, NULL)) {
		bail_out(EXIT_FAILURE, "sigaction()");
	}
}
