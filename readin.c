/**
 * @file readin.c
 * @author Jannik Vierling 1226434
 * @date 15.12.2013
 * 
 * @brief This program reads a file, line by line, strips all non alphabetical
 * charachters, capitalizes all remaining characters, and writes the result
 * in a shared memory segement.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>
#include <sem182.h>
#include "caesar.h"

/** 
 * @struct arguments
 * @brief Contains the command line arguments
 */
struct arguments {
	/** @var arguments::fin
     * Input file stream */
	FILE *fin;
	/** @var arguments::progname
	 * The programs name, as called from the command line */	
	char *progname; 
	/** @var arguments::infile
	 * Input file */
	char *infile;
}; 

/** @brief Holds the parsed command line arguments */
static struct arguments args = {.fin = NULL, .progname = "readin",
				.infile = NULL };
/**
 * @struct shm
 * @brief Contains data about a shared memory segment
 */
struct shm {
	/* @var shm::shmid
	 * The shared memory segments id */
	int shmid;
	/* @var shm::key
	 * The key the shared memory segment is associated with */
	key_t key;
	/* @var shm::shm
	 * Pointer to the address where the segment is attached */
	char * shm;
};

/** @brief The shared memory segment */
static struct shm shared = {.shmid = -1, .key = -1, .shm = NULL};

/** @brief ID of Semaphor 1 */
static int sem1 = -1;

/** @brief ID of Semaphor 2 */
static int sem2 = -1;

/**
 * @brief Prints an error message, and leaves the program with the code retval
 * @param retval The return code
 * @param fmt A format string for the error message
 * @return void
 * @details If errno is not equal to 0, this function also prints an errno
 * specific error message.
 */
static void bail_out(int retval, const char *fmt, ...);

/**
 * @brief Prints the synopsis of this program to stdout 
 * @return void
 */
static void usage(void);

/**
 * @brief Parses the command line arguments
 * @param argc The number of command line arguments
 * @param argv An array of command line arguments
 * @return void
 * @details saves the parsed arguments to ::args
 */
static void parse_args(int argc, char **argv);

/**
 * @brief Strips all non alphabetical character from str, and capitalizes the
 * remaining characters.
 * @param str The string to be stripped
 * @return A pointer on str[0]
 */
static char * strip_line(char *str);

/**
 * @brief Frees the ressources by this program
 * @param force If true all resources are freed regardless of synchronization
 * @return void
 * @details This function frees ::sem1, ::sem2, ::shared, ::fin
 */
static void free_resources(int force);

/**
 * @brief Initializes the shared memory segment
 * @return void
 * @details This function might create a new segment
 */
static void init_shm(void);

/**
 * @brief Initializes the semaphores
 * @return void
 * @details This function tries to grab existing semaphores, if the semaphores
 * do not exist it will create them.
 */
static void init_sem(void);

/**
 * @brief Initializes the signal handler
 * @return void
 * @details The signal handler is initilized for SIGINT and SIGTERM
 */
static void init_signal_handler(void);

/**
 * @brief Forces the program to terminate
 * @param sig The signal number that triggered the execution of this function.
 * @return void
 */
static void terminate(int sig);

/**
 * @brief The programs entry point. This function reads a line from fin,
 * writes the stripped line to the shared memory, waits for the caesar
 * program to read the line, then repeats these steps until end of file in fin
 * is reached.
 * @param argc The number of command line arguments.
 * @param argv An array of command line arguments.
 */
int main(int argc, char **argv)
{
	char line[MAX_LINE_LEN];

	init_signal_handler();

	parse_args(argc, argv);

	init_sem();
	init_shm();

	/* Open file */
	args.fin = fopen(args.infile, "r");

	if (NULL == args.fin) {
		bail_out(EXIT_FAILURE, "fopen()");
	}

	/* Copy lines to shared memory */
	while ( NULL != fgets(line, MAX_LINE_LEN, args.fin)) {

		errno = 0;

		if (-1 == P(sem1)) {
			bail_out(EXIT_FAILURE, "P()");
		}

		strncpy(shared.shm, strip_line(line), MAX_LINE_LEN);
		
		if (-1 == V(sem2)) {
			bail_out(EXIT_FAILURE, "V()");
		}
	}

	if (0 != errno) {
		bail_out(EXIT_FAILURE, "fgets()");
	}

	/* Copy eof-string to shared memory */
	if (-1 == P(sem1)) {
		bail_out(EXIT_FAILURE, "P()");
	}

	strncpy(shared.shm, EOFSTR, MAX_LINE_LEN);
		
	if (-1 == V(sem2)) {
		bail_out(EXIT_FAILURE, "V()");
	}
		
	free_resources(0);

	return EXIT_SUCCESS;
}

void init_sem(void)
{
	/* Create semaphors */
	if (-1 == (sem1 = semgrab(SEMKEY))) {
		if (-1 == (sem1 = seminit(SEMKEY, PERMISSION, 1))) {
			bail_out(EXIT_FAILURE, "seminit()");
		}
	}
	
	if (-1 == (sem2 = semgrab(SEMKEY+1))) {
		if (-1 == (sem2 = seminit(SEMKEY+1, PERMISSION, 0))) {
			bail_out(EXIT_FAILURE, "seminit()");
		}
	}
}

void init_shm(void)
{
	/* create key */
	shared.key = ftok(SHMPATH, SHMPROJ);
	
	if (-1 == shared.key) {
		bail_out(EXIT_FAILURE, "ftok()");
	}	
	
	/* create segment */
	shared.shmid = shmget(shared.key, MAX_LINE_LEN * sizeof(char),
		IPC_CREAT | PERMISSION);

	if (-1 == shared.shmid) {
		bail_out(EXIT_FAILURE, "shmget()");
	}

	/* attach segment */
	shared.shm = shmat(shared.shmid, NULL, 0);

	if ((void *)-1 == shared.shm) {
		bail_out(EXIT_FAILURE, "shmat()");
	}
}

void parse_args(int argc, char **argv)
{
	if (argc != 2) {
		usage();
		bail_out(EXIT_FAILURE, "Invalid arguments");
	}
	
	args.progname = argv[0];
	args.infile   = argv[1];
}

void bail_out(int retval, const char *fmt, ...)
{
	va_list ap;

	(void) fprintf(stderr, "%s : ", args.progname);

	va_start(ap, fmt);	
	(void) vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (errno != 0) {
		(void) fprintf(stderr, " : %s", strerror(errno));
	}

	(void) fputc('\n', stderr);

	free_resources(1);

	exit(retval);
}

void free_resources(int force)
{
	if (args.fin != NULL) {
		(void) fclose(args.fin);
	}

	if (!force) {
		(void) P(sem1);
	}

	if (-1 != shared.shmid) {
		(void) shmdt(shared.shm);
	}

	if (-1 != shared.shmid) {
		(void) shmctl(shared.shmid, IPC_RMID, NULL);
	}
	
	(void) semrm(sem1);
	(void) semrm(sem2);
}

void usage(void)
{
	(void) printf("usage : %s <filename>\n", args.progname);
}

char * strip_line(char *str)
{
	char * tmp = str;
	int j = 0;

	while (*str != '\0') {
		if (isalpha(*str)) {
			tmp[j++] = toupper(*str);
		}
		str++;
	}
	tmp[j] = '\0';

	return tmp;
}

void terminate(int sig)
{
	free_resources(1);
	
	(void) fprintf(stdout, "Caught signal %d. Terminating...\n",
			sig);
	
	exit(EXIT_FAILURE);
}

void init_signal_handler(void)
{
	struct sigaction sa;

	sa.sa_handler = terminate;
	
	if (-1 == sigemptyset(&(sa.sa_mask))) {
		bail_out(EXIT_FAILURE, "sigemptyset()");
	}

	if (-1 == sigaction(SIGINT, &sa, NULL)) {
		bail_out(EXIT_FAILURE, "sigaction()");
	}
	if (-1 == sigaction(SIGTERM, &sa, NULL)) {
		bail_out(EXIT_FAILURE, "sigaction()");
	}
}
