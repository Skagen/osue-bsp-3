# Makefile for the programs readin and caesar
# Author: Jannik Vierling 1226434
# Date: 15.12.2013

CFLAGS=-std=c99 -Wall -g -pedantic -DENDEBUG -D_BSD_SOURCE -D_XOPEN_SOURCE=500
CC=gcc
LDFLAGS=-lsem182
EXEC=caesar readin

all: $(EXEC)

readin: readin.o
	$(CC) $< -o $@ $(LDFLAGS)

caesar: caesar.o
	$(CC) $< -o $@ $(LDFLAGS)

%o:%.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean

clean:
	rm -rf $(EXEC)
	rm -rf *.o
