/**
 * @file caesar.h
 * @author Jannik Vierling 1226434
 * @date 15.12.2013
 * 
 * @brief Contains some constants required by readin.c and caesar.c
 */

#ifndef DEF_CAESAR_H
#define DEF_CAESAR_H

/** @brief The maximum length of a line read in the inputfile */
#define MAX_LINE_LEN (1022)

/** @brief This is used to generate the shared memory key */
#define SHMPATH "."

/** @brief This is used to generate the shared memory key */
#define SHMPROJ (05)

/** @brief Permissions for the shared memory segment, and the semaphors */
#define PERMISSION (0600)

/** @brief The string that is used to signal caesar the end of input */
#define EOFSTR "ende"

/** @brief The key to identifiy the semaphores */
#define SEMKEY (0xdeadbeef)

#endif
